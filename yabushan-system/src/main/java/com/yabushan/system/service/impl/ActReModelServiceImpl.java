package com.yabushan.system.service.impl;

import java.util.List;
import com.yabushan.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.yabushan.system.mapper.ActReModelMapper;
import com.yabushan.system.domain.ActReModel;
import com.yabushan.system.service.IActReModelService;

/**
 * 工作流模型Service业务层处理
 *
 * @author yabushan
 * @date 2021-04-21
 */
@Service
public class ActReModelServiceImpl implements IActReModelService
{
    @Autowired
    private ActReModelMapper actReModelMapper;

    /**
     * 查询工作流模型
     *
     * @param id 工作流模型ID
     * @return 工作流模型
     */
    @Override
    public ActReModel selectActReModelById(String id)
    {
        return actReModelMapper.selectActReModelById(id);
    }

    /**
     * 查询工作流模型列表
     *
     * @param actReModel 工作流模型
     * @return 工作流模型
     */
    @Override
    public List<ActReModel> selectActReModelList(ActReModel actReModel)
    {
        return actReModelMapper.selectActReModelList(actReModel);
    }

    /**
     * 新增工作流模型
     *
     * @param actReModel 工作流模型
     * @return 结果
     */
    @Override
    public int insertActReModel(ActReModel actReModel)
    {
        actReModel.setCreateTime(DateUtils.getNowDate());
        return actReModelMapper.insertActReModel(actReModel);
    }

    /**
     * 修改工作流模型
     *
     * @param actReModel 工作流模型
     * @return 结果
     */
    @Override
    public int updateActReModel(ActReModel actReModel)
    {
        return actReModelMapper.updateActReModel(actReModel);
    }

    /**
     * 批量删除工作流模型
     *
     * @param ids 需要删除的工作流模型ID
     * @return 结果
     */
    @Override
    public int deleteActReModelByIds(String[] ids)
    {
        return actReModelMapper.deleteActReModelByIds(ids);
    }

    /**
     * 删除工作流模型信息
     *
     * @param id 工作流模型ID
     * @return 结果
     */
    @Override
    public int deleteActReModelById(String id)
    {
        return actReModelMapper.deleteActReModelById(id);
    }
}
