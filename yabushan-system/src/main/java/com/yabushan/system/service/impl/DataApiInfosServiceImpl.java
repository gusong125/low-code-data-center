package com.yabushan.system.service.impl;

import java.util.List;
import com.yabushan.common.utils.DateUtils;
import com.yabushan.common.utils.SecurityUtils;
import com.yabushan.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.yabushan.system.mapper.DataApiInfosMapper;
import com.yabushan.system.domain.DataApiInfos;
import com.yabushan.system.service.IDataApiInfosService;

/**
 * 数据服务APIService业务层处理
 *
 * @author yabushan
 * @date 2021-03-28
 */
@Service
public class DataApiInfosServiceImpl implements IDataApiInfosService
{
    @Autowired
    private DataApiInfosMapper dataApiInfosMapper;

    /**
     * 查询数据服务API
     *
     * @param id 数据服务APIID
     * @return 数据服务API
     */
    @Override
    public DataApiInfos selectDataApiInfosById(Long id)
    {
        return dataApiInfosMapper.selectDataApiInfosById(id);
    }

    /**
     * 查询数据服务API列表
     *
     * @param dataApiInfos 数据服务API
     * @return 数据服务API
     */
    @Override
    public List<DataApiInfos> selectDataApiInfosList(DataApiInfos dataApiInfos)
    {
            if(!SecurityUtils.getUsername().equals("admin")) {
                dataApiInfos.setDatasourceId(SecurityUtils.getLoginUser().getUser().getUserId() + "");
            }
        return dataApiInfosMapper.selectDataApiInfosList(dataApiInfos);
    }

    @Override
    public List<DataApiInfos> AppselectDataApiInfosList(DataApiInfos dataApiInfos) {
        return dataApiInfosMapper.selectDataApiInfosList(dataApiInfos);
    }

    /**
     * 新增数据服务API
     *
     * @param dataApiInfos 数据服务API
     * @return 结果
     */
    @Override
    public int insertDataApiInfos(DataApiInfos dataApiInfos)
    {
        dataApiInfos.setCreateTime(DateUtils.getNowDate());
        dataApiInfos.setCreateBy(SecurityUtils.getUsername());
        dataApiInfos.setUpdateBy(SecurityUtils.getUsername());
        dataApiInfos.setUpdateTime(DateUtils.getNowDate());
        dataApiInfos.setResourceId(StringUtils.getUUID());
        dataApiInfos.setResourceCode(StringUtils.getUUID());
        dataApiInfos.setApiCode(StringUtils.getUUID());
        return dataApiInfosMapper.insertDataApiInfos(dataApiInfos);
    }

    /**
     * 修改数据服务API
     *
     * @param dataApiInfos 数据服务API
     * @return 结果
     */
    @Override
    public int updateDataApiInfos(DataApiInfos dataApiInfos)
    {
        dataApiInfos.setUpdateTime(DateUtils.getNowDate());
        dataApiInfos.setUpdateBy(SecurityUtils.getUsername());
        return dataApiInfosMapper.updateDataApiInfos(dataApiInfos);
    }

    /**
     * 批量删除数据服务API
     *
     * @param ids 需要删除的数据服务APIID
     * @return 结果
     */
    @Override
    public int deleteDataApiInfosByIds(Long[] ids)
    {
        return dataApiInfosMapper.deleteDataApiInfosByIds(ids);
    }

    /**
     * 删除数据服务API信息
     *
     * @param id 数据服务APIID
     * @return 结果
     */
    @Override
    public int deleteDataApiInfosById(Long id)
    {
        return dataApiInfosMapper.deleteDataApiInfosById(id);
    }
}
