package com.yabushan.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.system.domain.EmpSubComputer;
import com.yabushan.system.service.IEmpSubComputerService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 员工计算机能力子集Controller
 *
 * @author yabushan
 * @date 2021-03-21
 */
@RestController
@RequestMapping("/system/computer")
public class EmpSubComputerController extends BaseController
{
    @Autowired
    private IEmpSubComputerService empSubComputerService;

    /**
     * 查询员工计算机能力子集列表
     */
    @PreAuthorize("@ss.hasPermi('system:computer:list')")
    @GetMapping("/list")
    public TableDataInfo list(EmpSubComputer empSubComputer)
    {
        startPage();
        List<EmpSubComputer> list = empSubComputerService.selectEmpSubComputerList(empSubComputer);
        return getDataTable(list);
    }

    /**
     * 导出员工计算机能力子集列表
     */
    @PreAuthorize("@ss.hasPermi('system:computer:export')")
    @Log(title = "员工计算机能力子集", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(EmpSubComputer empSubComputer)
    {
        List<EmpSubComputer> list = empSubComputerService.selectEmpSubComputerList(empSubComputer);
        ExcelUtil<EmpSubComputer> util = new ExcelUtil<EmpSubComputer>(EmpSubComputer.class);
        return util.exportExcel(list, "computer");
    }

    /**
     * 获取员工计算机能力子集详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:computer:query')")
    @GetMapping(value = "/{recId}")
    public AjaxResult getInfo(@PathVariable("recId") String recId)
    {
        return AjaxResult.success(empSubComputerService.selectEmpSubComputerById(recId));
    }

    /**
     * 新增员工计算机能力子集
     */
    @PreAuthorize("@ss.hasPermi('system:computer:add')")
    @Log(title = "员工计算机能力子集", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody EmpSubComputer empSubComputer)
    {
        return toAjax(empSubComputerService.insertEmpSubComputer(empSubComputer));
    }

    /**
     * 修改员工计算机能力子集
     */
    @PreAuthorize("@ss.hasPermi('system:computer:edit')")
    @Log(title = "员工计算机能力子集", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody EmpSubComputer empSubComputer)
    {
        return toAjax(empSubComputerService.updateEmpSubComputer(empSubComputer));
    }

    /**
     * 删除员工计算机能力子集
     */
    @PreAuthorize("@ss.hasPermi('system:computer:remove')")
    @Log(title = "员工计算机能力子集", businessType = BusinessType.DELETE)
	@DeleteMapping("/{recIds}")
    public AjaxResult remove(@PathVariable String[] recIds)
    {
        return toAjax(empSubComputerService.deleteEmpSubComputerByIds(recIds));
    }
}
