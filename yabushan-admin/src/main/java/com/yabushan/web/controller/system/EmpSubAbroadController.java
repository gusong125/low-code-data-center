package com.yabushan.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.system.domain.EmpSubAbroad;
import com.yabushan.system.service.IEmpSubAbroadService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 员工出国出境子集Controller
 *
 * @author yabushan
 * @date 2021-03-21
 */
@RestController
@RequestMapping("/system/abroad")
public class EmpSubAbroadController extends BaseController
{
    @Autowired
    private IEmpSubAbroadService empSubAbroadService;

    /**
     * 查询员工出国出境子集列表
     */
    @PreAuthorize("@ss.hasPermi('system:abroad:list')")
    @GetMapping("/list")
    public TableDataInfo list(EmpSubAbroad empSubAbroad)
    {
        startPage();
        List<EmpSubAbroad> list = empSubAbroadService.selectEmpSubAbroadList(empSubAbroad);
        return getDataTable(list);
    }

    /**
     * 导出员工出国出境子集列表
     */
    @PreAuthorize("@ss.hasPermi('system:abroad:export')")
    @Log(title = "员工出国出境子集", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(EmpSubAbroad empSubAbroad)
    {
        List<EmpSubAbroad> list = empSubAbroadService.selectEmpSubAbroadList(empSubAbroad);
        ExcelUtil<EmpSubAbroad> util = new ExcelUtil<EmpSubAbroad>(EmpSubAbroad.class);
        return util.exportExcel(list, "abroad");
    }

    /**
     * 获取员工出国出境子集详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:abroad:query')")
    @GetMapping(value = "/{recId}")
    public AjaxResult getInfo(@PathVariable("recId") String recId)
    {
        return AjaxResult.success(empSubAbroadService.selectEmpSubAbroadById(recId));
    }

    /**
     * 新增员工出国出境子集
     */
    @PreAuthorize("@ss.hasPermi('system:abroad:add')")
    @Log(title = "员工出国出境子集", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody EmpSubAbroad empSubAbroad)
    {
        return toAjax(empSubAbroadService.insertEmpSubAbroad(empSubAbroad));
    }

    /**
     * 修改员工出国出境子集
     */
    @PreAuthorize("@ss.hasPermi('system:abroad:edit')")
    @Log(title = "员工出国出境子集", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody EmpSubAbroad empSubAbroad)
    {
        return toAjax(empSubAbroadService.updateEmpSubAbroad(empSubAbroad));
    }

    /**
     * 删除员工出国出境子集
     */
    @PreAuthorize("@ss.hasPermi('system:abroad:remove')")
    @Log(title = "员工出国出境子集", businessType = BusinessType.DELETE)
	@DeleteMapping("/{recIds}")
    public AjaxResult remove(@PathVariable String[] recIds)
    {
        return toAjax(empSubAbroadService.deleteEmpSubAbroadByIds(recIds));
    }
}
