package com.yabushan.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.system.domain.DataApiLog;
import com.yabushan.system.service.IDataApiLogService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * api日志Controller
 *
 * @author yabushan
 * @date 2021-06-13
 */
@RestController
@RequestMapping("/system/apidatalog")
public class DataApiLogController extends BaseController
{
    @Autowired
    private IDataApiLogService dataApiLogService;

    /**
     * 查询api日志列表
     */
    @PreAuthorize("@ss.hasPermi('system:apidatalog:list')")
    @GetMapping("/list")
    public TableDataInfo list(DataApiLog dataApiLog)
    {
        startPage();
        List<DataApiLog> list = dataApiLogService.selectDataApiLogList(dataApiLog);
        return getDataTable(list);
    }

    /**
     * 导出api日志列表
     */
    @PreAuthorize("@ss.hasPermi('system:apidatalog:export')")
    @Log(title = "api日志", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DataApiLog dataApiLog)
    {
        List<DataApiLog> list = dataApiLogService.selectDataApiLogList(dataApiLog);
        ExcelUtil<DataApiLog> util = new ExcelUtil<DataApiLog>(DataApiLog.class);
        return util.exportExcel(list, "apidatalog");
    }

    /**
     * 获取api日志详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:apidatalog:query')")
    @GetMapping(value = "/{logId}")
    public AjaxResult getInfo(@PathVariable("logId") Long logId)
    {
        return AjaxResult.success(dataApiLogService.selectDataApiLogById(logId));
    }

    /**
     * 新增api日志
     */
    @PreAuthorize("@ss.hasPermi('system:apidatalog:add')")
    @Log(title = "api日志", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DataApiLog dataApiLog)
    {
        return toAjax(dataApiLogService.insertDataApiLog(dataApiLog));
    }

    /**
     * 修改api日志
     */
    @PreAuthorize("@ss.hasPermi('system:apidatalog:edit')")
    @Log(title = "api日志", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DataApiLog dataApiLog)
    {
        return toAjax(dataApiLogService.updateDataApiLog(dataApiLog));
    }

    /**
     * 删除api日志
     */
    @PreAuthorize("@ss.hasPermi('system:apidatalog:remove')")
    @Log(title = "api日志", businessType = BusinessType.DELETE)
	@DeleteMapping("/{logIds}")
    public AjaxResult remove(@PathVariable Long[] logIds)
    {
        return toAjax(dataApiLogService.deleteDataApiLogByIds(logIds));
    }
}
