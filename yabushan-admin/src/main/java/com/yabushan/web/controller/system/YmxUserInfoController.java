package com.yabushan.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.system.domain.YmxUserInfo;
import com.yabushan.system.service.IYmxUserInfoService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 用户Controller
 *
 * @author yabushan
 * @date 2021-04-02
 */
@RestController
@RequestMapping("/ymx/ymxuserInfo")
public class YmxUserInfoController extends BaseController
{
    @Autowired
    private IYmxUserInfoService ymxUserInfoService;

    /**
     * 查询用户列表
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxuserInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(YmxUserInfo ymxUserInfo)
    {
        startPage();
        List<YmxUserInfo> list = ymxUserInfoService.selectYmxUserInfoList(ymxUserInfo);
        return getDataTable(list);
    }

    /**
     * 导出用户列表
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxuserInfo:export')")
    @Log(title = "用户", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(YmxUserInfo ymxUserInfo)
    {
        List<YmxUserInfo> list = ymxUserInfoService.selectYmxUserInfoList(ymxUserInfo);
        ExcelUtil<YmxUserInfo> util = new ExcelUtil<YmxUserInfo>(YmxUserInfo.class);
        return util.exportExcel(list, "ymxuserInfo");
    }

    /**
     * 获取用户详细信息
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxuserInfo:query')")
    @GetMapping(value = "/{userId}")
    public AjaxResult getInfo(@PathVariable("userId") String userId)
    {
        return AjaxResult.success(ymxUserInfoService.selectYmxUserInfoById(userId));
    }

    /**
     * 新增用户
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxuserInfo:add')")
    @Log(title = "用户", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody YmxUserInfo ymxUserInfo)
    {
        return toAjax(ymxUserInfoService.insertYmxUserInfo(ymxUserInfo));
    }

    /**
     * 修改用户
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxuserInfo:edit')")
    @Log(title = "用户", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody YmxUserInfo ymxUserInfo)
    {
        return toAjax(ymxUserInfoService.updateYmxUserInfo(ymxUserInfo));
    }

    /**
     * 删除用户
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxuserInfo:remove')")
    @Log(title = "用户", businessType = BusinessType.DELETE)
	@DeleteMapping("/{userIds}")
    public AjaxResult remove(@PathVariable String[] userIds)
    {
        return toAjax(ymxUserInfoService.deleteYmxUserInfoByIds(userIds));
    }
}
