import request from '@/utils/request'

// 查询附件列表
export function listYmxfileinfo(query) {
  return request({
    url: '/ymx/ymxfileinfo/list',
    method: 'get',
    params: query
  })
}

// 查询附件详细
export function getYmxfileinfo(fileId) {
  return request({
    url: '/ymx/ymxfileinfo/' + fileId,
    method: 'get'
  })
}

// 新增附件
export function addYmxfileinfo(data) {
  return request({
    url: '/ymx/ymxfileinfo',
    method: 'post',
    data: data
  })
}

// 修改附件
export function updateYmxfileinfo(data) {
  return request({
    url: '/ymx/ymxfileinfo',
    method: 'put',
    data: data
  })
}

// 删除附件
export function delYmxfileinfo(fileId) {
  return request({
    url: '/ymx/ymxfileinfo/' + fileId,
    method: 'delete'
  })
}

// 导出附件
export function exportYmxfileinfo(query) {
  return request({
    url: '/ymx/ymxfileinfo/export',
    method: 'get',
    params: query
  })
}